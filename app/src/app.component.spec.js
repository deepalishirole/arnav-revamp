"use strict";
var testing_1 = require('@angular/core/testing');
var testing_2 = require('@angular/router/testing');
var platform_browser_1 = require('@angular/platform-browser');
var app_component_1 = require('./app.component');
var shared_module_1 = require('./components/_shared/shared.module');
var auth_guard_1 = require('./common/auth.guard');
describe('AppComponent', function () {
    var de;
    var comp;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                app_component_1.AppComponent
            ],
            imports: [
                shared_module_1.SharedModule,
                testing_2.RouterTestingModule
            ],
            providers: [
                auth_guard_1.AUTH_PROVIDERS
            ]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(app_component_1.AppComponent);
            comp = fixture.componentInstance;
            de = fixture.debugElement.query(platform_browser_1.By.css('h1.header-title'));
        });
    }));
    it('should create component', function () { return expect(comp).toBeDefined(); });
    it('should have expected <h1> text', function () {
        fixture.detectChanges();
        var h1 = de.nativeElement;
        expect(h1.innerText).toEqual('Angular 2 Seed');
    });
});
