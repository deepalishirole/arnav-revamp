"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var auth_guard_1 = require('./common/auth.guard');
var routes_1 = require('./routes');
var shared_module_1 = require('./components/_shared/shared.module');
var app_component_1 = require('./app.component');
var dashboard_module_1 = require('./components/dashboard/dashboard.module');
var error_module_1 = require('./components/error/error.module');
var home_module_1 = require('./components/home/home.module');
var login_module_1 = require('./components/login/login.module');
var side_navbar_component_1 = require('./components/side-navbar/side-navbar.component');
var enquiry_component_1 = require('./components/enquiry/enquiry.component');
var http_1 = require('@angular/http');
var forms_1 = require('@angular/forms');
var mailsending_service_1 = require('./services/mailsending.service');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                shared_module_1.SharedModule,
                dashboard_module_1.DashboardModule,
                error_module_1.ErrorModule,
                home_module_1.HomeModule,
                login_module_1.LoginModule,
                http_1.HttpModule,
                forms_1.FormsModule,
                routes_1.routing
            ],
            declarations: [
                app_component_1.AppComponent, side_navbar_component_1.SideNavbarComponent, enquiry_component_1.EnquiryComponent
            ],
            providers: [
                auth_guard_1.AUTH_PROVIDERS, mailsending_service_1.MailsendingService
            ],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
