import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AUTH_PROVIDERS } from './common/auth.guard';
import { routing } from './routes';
import { SharedModule } from './components/_shared/shared.module';

import { AppComponent } from './app.component';
import { DashboardModule } from './components/dashboard/dashboard.module';
import { ErrorModule } from './components/error/error.module';
import { HomeModule } from './components/home/home.module';
import { LoginModule } from './components/login/login.module';
import {SideNavbarComponent} from './components/side-navbar/side-navbar.component';
import {EnquiryComponent} from './components/enquiry/enquiry.component';
/*import {BlogsService} from './components/blogs/blogs.service';*/
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import {MailsendingService} from './services/mailsending.service';

@NgModule({
    imports: [
        BrowserModule,
        SharedModule,
        DashboardModule,
        ErrorModule,
        HomeModule,
        LoginModule,
        HttpModule,
        FormsModule,
        routing
    ],
    declarations: [
        AppComponent, SideNavbarComponent, EnquiryComponent
    ],
    providers: [
        AUTH_PROVIDERS, MailsendingService
    ],
    bootstrap: [AppComponent]
})

export class AppModule {}
