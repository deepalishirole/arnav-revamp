import {Component} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'about-mission',
    templateUrl: 'about-mission.component.html',
    styleUrls: ['about-mission.component.css']
})
export class AboutMissionComponent {
}
