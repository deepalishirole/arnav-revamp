import {Component} from '@angular/core';
@Component({
    selector: 'about',
    templateUrl: 'about.component.html',
    styleUrls: ['about.component.css'],
    moduleId: module.id
})
export class AboutComponent {
    team1: string;
    team2: string;
    team3: string;
    constructor() {
        this.team1 = './src/images/admin.jpg';
        this.team2 = './src/images/admin.jpg';
        this.team3 = './src/images/admin.jpg';
    }
}
