import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'angular2-courses',
    templateUrl: 'angular2.component.html',
    styleUrls: ['course.component.css']
})

export class Angular2Component {
    public img: string;

    constructor() {
        this.img = './src/images/img4.png';
    }

}
