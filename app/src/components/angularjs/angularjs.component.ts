import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'angularjs-courses',
    templateUrl: 'angularjs.component.html',
    styleUrls: ['angularjs.component.css']
})

export class AngularjsComponent {
    public img: string;

    constructor() {
        this.img = './src/images/img4.png';
    }

}
