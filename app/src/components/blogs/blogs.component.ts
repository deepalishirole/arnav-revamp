import {Component} from '@angular/core';
/*import {BlogsService} from './blogs.service';*/

@Component({
    selector: 'blog',
    templateUrl: 'blogs.component.html',
    styleUrls: ['blogs.component.css'],
    moduleId: module.id
})
export class BlogsComponent {
    image1: string;
    image2: string;
    image3: string;
    image4: string;
    angular2: string;
   self: string;
    constructor() {
        this.image1 = './src/images/banner-class.jpg';
        this.image2 = './src/images/banner-class.jpg';
        this.image3 = './src/images/banner-class.jpg';
        this.image4 = './src/images/banner-class.jpg';
        this.angular2 = './src/images/banner-class.jpg';
        this.self = './src/images/admin.jpg';
    }
}
