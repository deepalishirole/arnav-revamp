import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class BlogsService {
    constructor (private _http: Http) {}
    getBlogs() {
        return this._http.get('blogs.json').map((res: Response) => res.json());
    }
}
