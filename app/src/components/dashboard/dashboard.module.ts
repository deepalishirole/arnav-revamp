import { NgModule } from '@angular/core';

/*import { SharedModule } from '../_shared/shared.module';*/
import { DashboardComponent } from './dashboard.component';

@NgModule({
    imports: [],
    declarations: [DashboardComponent]
})

export class DashboardModule {}
