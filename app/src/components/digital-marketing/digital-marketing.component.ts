import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'digital',
    templateUrl: 'digital-marketing.component.html',
    styleUrls: ['digital-marketing.component.css']
})

export class DigitalMarketingComponent {
    public img: string;

    constructor() {
        this.img = './src/images/img4.png';
    }

}
