import {Component} from '@angular/core';
import {MailsendingService} from '../../services/mailsending.service';

@Component ({
    selector: 'my-enquiry',
    templateUrl: 'enquiry.component.html',
    /*styleUrls: ['enquiry.component.css'],*/
    moduleId: module.id
})
export class EnquiryComponent {
    firstName: string;
    email: string;
    course: string;
    res: Object;
    constructor(private mail1: MailsendingService) {}

    /*ngOnInit() {
        this.res = {
            email: this.email,
            name: this.firstName
        };
    }*/

   /* submitDetail() {
        console.log(this.res);
    }*/
    addUser() {
        this.mail1.addNew(this.email, this.firstName);
    }
}
