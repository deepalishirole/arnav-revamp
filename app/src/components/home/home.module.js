"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var shared_module_1 = require('../_shared/shared.module');
var home_component_1 = require('./home.component');
var about_component_1 = require('../about/about.component');
var contact_component_1 = require('../contact/contact.component');
var blogs_component_1 = require('../blogs/blogs.component');
var who_we_r_component_1 = require('../home-who-we-r.component/who-we-r.component');
var new_courses_component_1 = require('../new- courses/new-courses.component');
var reviews_component_1 = require('../student-reviews/reviews.component');
var testimonials_component_1 = require('../testimonials/testimonials.component');
var angular2_component_1 = require('../angular2/angular2.component');
var about_mission_component_1 = require('../about-mision/about-mission.component');
var digital_marketing_component_1 = require('../digital-marketing/digital-marketing.component');
var angularjs_component_1 = require('../angularjs/angularjs.component');
var image_slider1_component_1 = require('../image-slider1/image-slider1.component');
var HomeModule = (function () {
    function HomeModule() {
    }
    HomeModule = __decorate([
        core_1.NgModule({
            imports: [shared_module_1.SharedModule],
            declarations: [home_component_1.HomeComponent, image_slider1_component_1.ImageSlider1Component, about_component_1.AboutComponent, reviews_component_1.ReviewsComponent, about_mission_component_1.AboutMissionComponent,
                contact_component_1.ContactComponent, blogs_component_1.BlogsComponent, angular2_component_1.Angular2Component, who_we_r_component_1.WhoWeRComponent, new_courses_component_1.NewCoursesComponent, testimonials_component_1.TestimonialsComponent,
                digital_marketing_component_1.DigitalMarketingComponent, angularjs_component_1.AngularjsComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], HomeModule);
    return HomeModule;
}());
exports.HomeModule = HomeModule;
