import { NgModule } from '@angular/core';
import { SharedModule } from '../_shared/shared.module';
import { HomeComponent } from './home.component';
import {AboutComponent} from '../about/about.component';
import {ContactComponent} from '../contact/contact.component';
import {BlogsComponent} from '../blogs/blogs.component';
import {WhoWeRComponent} from '../home-who-we-r.component/who-we-r.component';
import {NewCoursesComponent} from '../new- courses/new-courses.component';
import {ReviewsComponent} from '../student-reviews/reviews.component';
import {TestimonialsComponent} from '../testimonials/testimonials.component';
import {Angular2Component} from '../angular2/angular2.component';
import {AboutMissionComponent} from '../about-mision/about-mission.component';
import {DigitalMarketingComponent} from '../digital-marketing/digital-marketing.component';
import {AngularjsComponent} from '../angularjs/angularjs.component';
import {ImageSlider1Component} from '../image-slider1/image-slider1.component';

@NgModule({
    imports: [SharedModule],
    declarations: [HomeComponent, ImageSlider1Component, AboutComponent, ReviewsComponent, AboutMissionComponent,
        ContactComponent, BlogsComponent, Angular2Component, WhoWeRComponent, NewCoursesComponent, TestimonialsComponent,
        DigitalMarketingComponent, AngularjsComponent]
})
export class HomeModule {}
