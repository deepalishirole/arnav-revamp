"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ImageSlider1Component = (function () {
    function ImageSlider1Component() {
        this.image11 = './src/images/slider/333.jpg';
        this.image12 = './src/images/slider/444.png';
        this.image13 = './src/images/slider/mean.png';
        this.image14 = './src/images/slider/seo-banner.jpg';
    }
    ImageSlider1Component = __decorate([
        core_1.Component({
            selector: 'app-slider',
            templateUrl: 'image-slider1.component.html',
            styleUrls: ['image-slider1.component.css'],
            moduleId: module.id
        }), 
        __metadata('design:paramtypes', [])
    ], ImageSlider1Component);
    return ImageSlider1Component;
}());
exports.ImageSlider1Component = ImageSlider1Component;
