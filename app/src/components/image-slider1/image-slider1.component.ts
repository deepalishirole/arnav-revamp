import {Component} from '@angular/core';

@Component({
    selector: 'app-slider',
    templateUrl: 'image-slider1.component.html',
    styleUrls: ['image-slider1.component.css'],
    moduleId: module.id

})

export class ImageSlider1Component {
    image11: string;
    image12: string;
    image13: string;
    image14: string;
    constructor() {
        this.image11 = './src/images/software.png';
        this.image12 = './src/images/1digitalslider.jpg';
        this.image13 = './src/images/slider/mean.png';
        this.image14 = './src/images/web_developer.jpg';
    }
}
