"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var auth_service_1 = require('../../services/auth.service');
var form_state_model_1 = require('../../models/form/form-state.model');
var login_request_model_1 = require('../../models/auth/login-request.model');
var LoginComponent = (function () {
    function LoginComponent(_formBuilder, _router, _authService) {
        var _this = this;
        this._formBuilder = _formBuilder;
        this._router = _router;
        this._authService = _authService;
        if (this._authService.isUserAuthorized()) {
            this._router.navigate(['/dashboard']);
        }
        this.formState = new form_state_model_1.FormStateModel();
        this.loginForm = this._formBuilder.group({
            'username': ['', forms_1.Validators.required],
            'password': ['', forms_1.Validators.required]
        });
        this.loginForm.valueChanges.subscribe(function () {
            _this.formState.submitError = false;
        });
    }
    LoginComponent.prototype.onLoginFormSubmit = function () {
        var _this = this;
        this._loginRequest = new login_request_model_1.LoginRequestModel();
        this._loginRequest.username = this.loginForm.value.username;
        this._loginRequest.password = this.loginForm.value.password;
        this.formState.submitting = true;
        this.formState.submitError = false;
        this._authService.login(this._loginRequest).subscribe(function () {
            _this._router.navigate(['/dashboard']);
        }, function (errorMessage) {
            _this.formState.submitting = false;
            _this.formState.submitError = true;
            _this.formState.submitErrorMessage = errorMessage;
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'login',
            templateUrl: './login.component.html'
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, router_1.Router, auth_service_1.AuthService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
