"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var NewCoursesComponent = (function () {
    function NewCoursesComponent() {
        this.mongologo = './src/images/mongo.jpg';
        this.angularlogo = './src/images/mean-logo1.jpg';
        this.meanlogo = './src/images/mean-logo1.jpg';
        this.nodelogo = './src/images/nodejs.jpg';
    }
    NewCoursesComponent = __decorate([
        core_1.Component({
            selector: 'new-course',
            templateUrl: 'new-courses.component.html',
            styleUrls: ['new-courses.component.css'],
            moduleId: module.id
        }), 
        __metadata('design:paramtypes', [])
    ], NewCoursesComponent);
    return NewCoursesComponent;
}());
exports.NewCoursesComponent = NewCoursesComponent;
