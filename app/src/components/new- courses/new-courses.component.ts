import {Component} from '@angular/core';
@Component({
    selector: 'new-course',
    templateUrl: 'new-courses.component.html',
    styleUrls: ['new-courses.component.css'],
    moduleId: module.id
})
export class NewCoursesComponent {
    meanlogo: string;
    angularlogo: string;
    nodelogo: string;
    mongologo: string;
    constructor() {
        this.mongologo = './src/images/mongo.jpg';
        this.angularlogo = './src/images/mean-logo1.jpg';
        this.meanlogo = './src/images/mean-logo1.jpg';
        this.nodelogo = './src/images/nodejs.jpg';
    }
}
