import {Component} from '@angular/core';
@Component({
    selector: 'review-slider',
    templateUrl: 'reviews.component.html',
    styleUrls: ['../image-slider/image-slider.css'],
    moduleId: module.id
})
export class ReviewsComponent {
    rev: string;
    revImg  = [{url: './src/images/class1.jpg'},
        {url: './src/images/class12.jpg'},
        {url: './src/images/class13.jpg'},
        {url: './src/images/class14.jpg'},
        {url: './src/images/class15.jpg'}];
    constructor() {
        this.rev = './src/images/eccentric1.jpg';
    }
}
