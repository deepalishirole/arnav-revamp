"use strict";
(function (ResponseCode) {
    ResponseCode[ResponseCode["InvalidParam"] = 0] = "InvalidParam";
    ResponseCode[ResponseCode["InvalidCredentials"] = 1] = "InvalidCredentials";
    ResponseCode[ResponseCode["NotFound"] = 2] = "NotFound";
    ResponseCode[ResponseCode["SessionExpired"] = 3] = "SessionExpired";
    ResponseCode[ResponseCode["Conflict"] = 4] = "Conflict";
    ResponseCode[ResponseCode["InternalServerError"] = 5] = "InternalServerError";
})(exports.ResponseCode || (exports.ResponseCode = {}));
var ResponseCode = exports.ResponseCode;
