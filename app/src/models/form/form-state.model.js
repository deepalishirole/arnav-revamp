"use strict";
var FormStateModel = (function () {
    function FormStateModel() {
        this.submitting = false;
        this.submitError = false;
    }
    return FormStateModel;
}());
exports.FormStateModel = FormStateModel;
