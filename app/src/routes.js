"use strict";
var router_1 = require('@angular/router');
var auth_guard_1 = require('./common/auth.guard');
var home_component_1 = require('./components/home/home.component');
var login_component_1 = require('./components/login/login.component');
var dashboard_component_1 = require('./components/dashboard/dashboard.component');
var error_component_1 = require('./components/error/error.component');
var angular2_component_1 = require('./components/angular2/angular2.component');
var about_component_1 = require('./components/about/about.component');
var contact_component_1 = require('./components/contact/contact.component');
var blogs_component_1 = require('./components/blogs/blogs.component');
var digital_marketing_component_1 = require('./components/digital-marketing/digital-marketing.component');
var angularjs_component_1 = require('./components/angularjs/angularjs.component');
exports.routing = router_1.RouterModule.forRoot([
    {
        path: '',
        pathMatch: 'full',
        component: home_component_1.HomeComponent
    },
    {
        path: 'admin',
        pathMatch: 'prefix',
        component: login_component_1.LoginComponent
    },
    {
        path: 'angular2-courses',
        component: angular2_component_1.Angular2Component
    },
    {
        path: 'angularjs-courses',
        component: angularjs_component_1.AngularjsComponent
    },
    {
        path: 'digital',
        component: digital_marketing_component_1.DigitalMarketingComponent
    },
    {
        path: 'about',
        component: about_component_1.AboutComponent
    },
    {
        path: 'contact',
        component: contact_component_1.ContactComponent
    },
    {
        path: 'blog',
        component: blogs_component_1.BlogsComponent
    },
    {
        path: 'dashboard',
        component: dashboard_component_1.DashboardComponent,
        canActivate: [auth_guard_1.AuthGuard]
    },
    {
        path: 'error',
        component: error_component_1.ErrorComponent
    },
    {
        path: '**',
        redirectTo: 'error'
    }
]);
