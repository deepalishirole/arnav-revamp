import { RouterModule } from '@angular/router';

import { AuthGuard } from './common/auth.guard';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ErrorComponent } from './components/error/error.component';
import {Angular2Component} from './components/angular2/angular2.component';
import {AboutComponent} from './components/about/about.component';
import {ContactComponent} from './components/contact/contact.component';
import {BlogsComponent} from './components/blogs/blogs.component';
import {DigitalMarketingComponent} from './components/digital-marketing/digital-marketing.component';
import {AngularjsComponent} from './components/angularjs/angularjs.component';

export const routing = RouterModule.forRoot([
    {
        path: '',
        pathMatch: 'full',
        component: HomeComponent
    },
    {
        path: 'admin',
        pathMatch: 'prefix',
        component: LoginComponent
    },
    {
        path: 'angular2-courses',
        component: Angular2Component
    },
    {
        path: 'angularjs-courses',
        component: AngularjsComponent
    },
    {
        path: 'digital',
        component: DigitalMarketingComponent
    },
    {
        path: 'about',
        component: AboutComponent
    },
    {
        path: 'contact',
        component: ContactComponent
    },
    {
        path: 'blog',
        component: BlogsComponent
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
       // redirectTo: 'home',
        canActivate: [AuthGuard]
    },
    {
        path: 'error',
        component: ErrorComponent
    },
    {
        path: '**',
        redirectTo: 'error'
    }
]);
