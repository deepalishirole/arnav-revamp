"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Observable_1 = require('rxjs/Observable');
var AuthService = (function () {
    function AuthService() {
        this._isAuthorized = false;
    }
    AuthService.prototype.isUserAuthorized = function () {
        this._isAuthorized = false;
        this._sessionAuthData = JSON.parse(sessionStorage.getItem('authData'));
        if (this._sessionAuthData && this._sessionAuthData.token) {
            this._authorizeUser(this._sessionAuthData.token);
            this._isAuthorized = true;
        }
        return this._isAuthorized;
    };
    AuthService.prototype.login = function (loginRequest) {
        var _this = this;
        return Observable_1.Observable.create(function (subscriber) {
            if (loginRequest.username.toLowerCase() === 'admin' && loginRequest.password === '123') {
                _this._authorizeUser('json-web-token');
                subscriber.next();
                subscriber.complete();
            }
            else {
                subscriber.error('Invalid username or password');
            }
        });
    };
    AuthService.prototype.logout = function () {
        this._isAuthorized = false;
        sessionStorage.removeItem('authData');
    };
    AuthService.prototype._authorizeUser = function (token) {
        var authData = {
            token: token
        };
        this._isAuthorized = true;
        sessionStorage.setItem('authData', JSON.stringify(authData));
    };
    AuthService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;
