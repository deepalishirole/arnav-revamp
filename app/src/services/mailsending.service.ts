import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';

@Injectable()
export class MailsendingService {
    constructor(private http: Http) {
    }

    addNew(email: string, name: string) {
        let headers = new Headers();

        let request = 'email=' + email + '&name=' + name;
        headers.append('Content-Type', 'application/X-www-form-urlencoded');
        this.http.post('http://192.168.43.126/mailer/sendMail.php', request, {headers: headers})
            .subscribe((data) => {
            alert('successfully saved....' + data.json().error_msg);
            console.log('Sent successfully');

        });
    }
}
